// @flow
import config from './config'
import API from './api'
import axios from 'axios'
import Reject from './reject'
import * as stateTransform from './schema/state'
import * as jsonFormSchema from './schema/jsonFormSchema'
import * as formJSONTransform from './schema/formToWebaction'
import { getFormSchemaFromProperty } from './schema/webactionToFormSchema'

/**
 * Class representing a Web form.
 */
class WF {
  constructor () {
    /**
         * Lower level API for webforms
         * @type {API}
         */
    this.API = new API()
    /**
         * Custom promise of webforms
         * @type {Promise}
         */
    this.Promise = Promise
    /**
         *  requests object for the library | inherited from axios
         */
    this.axios = axios
  }

  /** Initialize your Webform
     * #### Example:
     * ```
     * WF.init({
     *  header_token: '<YOUR_APP_TOKEN>',
     *  client_id: '<YOUR_CLIENT_ID>',
     *  redirect_uri: '<CALLBACK_URL>'
     * })
     * ```
     *
     * @param {String} options.header_token     Header Token for the app
     * @param {String} options.client_id        Client Id for the application | Generates on initialization
     * @param {String} options.redirect_uri     Callback or Redirect url of the form | Optional
     */
  init (options = {}) {
    // set tokens
    config.set('header_token', options.header_token)
    config.set('client_id', options.client_id)
    config.set('redirect_uri', options.redirect_uri)
    config.set('baseURL', options.baseURL)
    config.set('name', options.name)
    return this.getSchema()
  }

  /**
   * Get All the forms for the given User
   * ### Usage
   * ```
   * // returns { Promise object with data }
   * WF.getForms().then(function(response){
   *      if(response.status == 200){
   *          console.log(response.data)
   *      }
   *  })
   * ```
   * @returns {Promise}
   */
  getForms (verbose = false) {
    const api = new API()
    const path = '/list'
    if (verbose) {
      api.get(path).then(function (response) {
        if (!response.data) {
          return Reject('No data available.')
        }
        response.data.WebActions.forEach(function (item) {
          if (item.ActionId && item.Name) {
            console.log(item.ActionId, item.Name)
          }
        })
      })
    }
    return api.get(path)
  }

  /**
   * Create a webForm .Returns a unique Id of the webform
   * #### Example:
   * ```
   * // returns _id {unique id} inside a promise
   * WF.create('<FORM_NAME>', { ...SchemaObject})
   * ```
   *
   * **Example SchemaObject:**
   * ```
   * let schemaObject = {
   *  'email': String,
   *  'subject': String,
   *  'message': String,
   *  'phone': Number
   * }
   *
   * ```
   *
   * @param {String} name  Name of the form | Unique
   * @param {Object} schema Schema Object for the javascript form
   * @returns {*}
   * @param {Object} transform Tranform object for plugins
   */
  create (name, schema, data, transform = {}) {
    if (!schema) {
      return new Reject('Schema not given. Please provide a Schema!')
    }
    config.set('name', name)
    const api = new API()
    const path = '/CreateOrUpdate'
    return api.post(path, {}, data)
  }

  /**
   * Gets the schema from the webform
   * @returns {Object}
   */
  getSchema () {
    const actionName = config.get('name')
    const _id = config.get('client_id')
    const schema = this.API.get(`/list?ActionId=${_id}`)
      .then(function (response) {
        debugger
        if (!response.data.WebActions) {
          return new Reject('Webform not found.')
        }

        let choosenWebAction = response.data.WebActions.filter(function (item) {
          if (actionName === item.Name) {
            return true
          } else if (_id === item.ActionId) {
            return true
          } else {
            return false
          }
        })

        choosenWebAction = choosenWebAction.pop()
        const webFormSchema = stateTransform.tranformWebActionToSchema(choosenWebAction)
        stateTransform.setSchema(webFormSchema)
        console.log('Webform Initialized!')
        return choosenWebAction
      }).catch(function (error) {
        return error
      })
    return schema
  }

  /**
   * Read Current Schema
   * Read Only
   * @returns {Object}
   */
  readSchema () {
    return stateTransform.readSchema()
  }

  getSchemaFromJsonForm (formSchema) {
    return jsonFormSchema.formToSchema(formSchema)
  }

  getJsonFormFromSchema (formSchema) {
    return getFormSchemaFromProperty(formSchema)
  }

  formToWebAction (jsonForm) {
    return formJSONTransform.formJSONToWebAction(jsonForm)
  }

}

export default new WF()
