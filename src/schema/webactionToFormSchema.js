import jsonpack from 'jsonpack'

export function getFormSchemaFromProperty (webAction) {
  const isEmpty = Object.keys(webAction).length === 0 && webAction.constructor === Object
  if (isEmpty) return []

  // meta data exists
  const metaExists = webAction.Properties.filter(function (prop) {
    if (prop && prop.PropertyName === 'metaFormSchema') {
      return prop
    }
  })[0]

  if (metaExists) {
    const schema = jsonpack.unpack(metaExists.DisplayName)
    return schema
  }

  const mappedObj = webAction.Properties.map(function (property) {
    switch (property.PropertyType) {
      case 0:
        return textField(property)
      case 1:
        return null// 'array'
      case 2:
        return numberField(property)
      case 3:
        return booleanField(property)
      case 4:
        return textField(property)
      case 5:
        return imageField(property)
      case 6:
        return linkField(property)
      default:
        return textField(property)
    }
  })
  return mappedObj
}

// helpers
function booleanField (propItem) {
  let item = {
    'type': 'checkbox-group',
    'required': propItem.IsRequired || false,
    'label': propItem.PropertyName,
    'name': propItem.DisplayName,
    'values': [
      {
        'label': 'Option 1',
        'value': 'option-1',
        'selected': true
      }
    ]
  }
  return item
}

function numberField (propItem) {
  let item = {
    'type': 'number',
    'required': propItem.IsRequired || false,
    'label': propItem.PropertyName,
    'placeholder': 'Enter number',
    'className': 'form-control',
    'name': propItem.DisplayName
  }
  return item
}

function textField (propItem) {
  let item = {
    'type': 'text',
    'required': propItem.IsRequired || false,
    'label': propItem.PropertyName,
    'placeholder': '',
    'className': 'form-control',
    'name': propItem.DisplayName,
    'subtype': 'text'
  }
  return item
}

function imageField (propItem) {
  let item = {
    'type': 'img',
    'required': propItem.IsRequired || false,
    'label': propItem.PropertyName,
    'placeholder': '',
    'className': 'form-control',
    'name': propItem.DisplayName
  }
  return item
}

function linkField (propItem) {
  let item = {
    'type': 'link',
    'required': propItem.IsRequired || false,
    'label': propItem.PropertyName,
    'placeholder': '',
    'className': 'form-control',
    'name': propItem.DisplayName
  }
  return item
}
