import jsonpack from 'jsonpack'

function getRegex (formProp) {
  if (formProp.subtype) {
    switch (formProp.subtype.toUpperCase()) {
      case 'EMAIL':
        return "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_" +
          '`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?'
      case 'TEL':
        return '\(?\d{3}\)?-? *\d{3}-? *-?\d{4}'
      case 'COLOR':
        return '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
      case 'PASSWORD':
        // Minimum eight characters, at least one letter and one number:
        return '^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$'
      case 'text':
        return null
      default:
        return null
    }
  }
}

function toCamelCase (str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}

export function formJSONToWebAction (formSchema) {
  if (formSchema.length === 0) return {}
  let webActions = formSchema.map(function (item, index) {
    let prop = {
      'DisplayName' : item.label,
      'PropertyName' : toCamelCase(item.label),
      'PropertyType' : getType(item),
      'IsRequired' : item.required || false,
      'ValidationRegex' : getRegex(item) || null
    }
    return prop
  })
  webActions = webActions.filter(function (item) {
    if (item.PropertyType) {
      return true
    }
  })
  webActions.push(addMeta(formSchema))
  return webActions
}

function addMeta (formSchema) {
  const metaPropertyDisplay = jsonpack.pack(formSchema)
  const prop = {
    'DisplayName': metaPropertyDisplay,
    'PropertyName': 'metaFormSchema',
    'PropertyType': 0,
    'DataType': null,
    'IsRequired': false,
    'ValidationRegex': null
  }
  return prop
}

function getFormfromMeta (prop) {
  const formSchema = jsonpack.unpack(prop.DisplayName)
  return formSchema
}

function getType (obj) {
  const isEmpty = Object.keys(obj).length === 0 && obj.constructor === Object
  if (isEmpty) return null
  switch (obj.type) {
    case 'text':
      return 'STR'
    case 'button':
      return null
    case 'Link':
      return 'LINK'
    case 'ImageUrl':
      return 'IMAGE'
    case 'checkbox-group':
      return 'boolean'
    case 'date':
      return 'STR'
    case 'number':
      return 'NUMBER'
    case 'select':
      return 'Array'
    case 'radio-group':
      return 'Array'
    case 'paragraph':
      return 'STR'
    case 'header':
      return 'STR'
    case 'textarea':
      return 'STR'
    case 'file':
      return 'STR'
    case 'autocomplete':
      return 'STR'
    default:
      return null
  }
}

export function WebActionToformJSON (obj) {
  const isEmpty = Object.keys(obj).length === 0 && obj.constructor === Object
  if (isEmpty) return null
  return obj
}
