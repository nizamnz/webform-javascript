/**
 * Singleton schema object for the sdk.
 */
export const currentSchema = {

}

/**
 * Methods for the config object.
 * @type {{get: ((key)), set: ((key, value?))}}
 */
module.exports = {
  read () {
    return { ...currentSchema }
  },

  clear() {
    for (let key in currentSchema){
      if (currentSchema.hasOwnProperty(key)){
        delete currentSchema[key];
      }
    }
  },

  get (key) {
    return currentSchema[key]
  },

  set (key, value) {
    if (value) {
      currentSchema[key] = value
    }
  }
}
