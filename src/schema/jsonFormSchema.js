import jsonpack from 'jsonpack'

function toCamelCase (str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}

function getFromMetaData (metaProp) {
  const formSchema = jsonpack.unpack(metaProp.DisplayName)

  console.log(formSchema)
}

export function formToSchema (formSchema) {
  const metaExists = formSchema.filter(function (item) {
    return item.label === 'metaFormSchema'
  })[0]

  if (metaExists) {
    return getFromMetaData(metaExists)
  }

  const isEmpty = Object.keys(formSchema).length === 0 && formSchema.constructor === Object
  if (isEmpty) return {}
  var res = {}
  formSchema.forEach(function (element) {
    switch (element.type) {
      case 'text':
        res[toCamelCase(element.label)] = String
        break
      case 'metaFormSchema':
        return getFromMetaData(element)
      case 'button':
        res[toCamelCase(element.label)] = null
        break
      case 'checkbox-group':
        res[toCamelCase(element.label)] = Array
        break
      case 'date':
        res[toCamelCase(element.label)] = String
        break
      case 'number':
        res[toCamelCase(element.label)] = Number
        break
      case 'select':
        res[toCamelCase(element.label)] = Array
        break
      case 'radio-group':
        res[toCamelCase(element.label)] = Array
        break
      case 'paragraph':
        res[toCamelCase(element.label)] = String
        break
      case 'header':
        res[toCamelCase(element.label)] = String
        break
      case 'textarea':
        res[toCamelCase(element.label)] = String
        break
      case 'file':
        res[toCamelCase(element.label)] = String
        break
      case 'autocomplete':
        res[toCamelCase(element.label)] = String
        break
      default:
        res[toCamelCase(element.label)] = String
    }
  })
  return res
}
