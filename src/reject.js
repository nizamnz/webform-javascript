/**
 * Error promise message for improper promise calls
 * @param errMsg
 * @returns {{}}
 */
export default function Reject (errMsg) {
  let rejection = {}

  rejection.promise = Promise.reject(new Error(errMsg)).then(function (error) {
    return null
  }, function (error) {
    console.log(error) // Stacktrace
    return error
  })

  return rejection
}
