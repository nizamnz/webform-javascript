/**
 * Singleton config object for the sdk.
 * @type {{header_token: string, baseURL: string, devURL: string, client_id: string, redirect_uri: string}}
 */
const config = {
  header_token: undefined,
  baseURL: 'http://api.kitsunedev.com',
  name: undefined,
  client_id: undefined,
  redirect_uri: undefined,
  api_version: 'v1'
}

/**
 * Methods for the config object.
 * @type {{get: ((key)), set: ((key, value?))}}
 */
module.exports = {
  get (key) {
    return config[key]
  },

  set (key, value) {
    if (value) {
      config[key] = value
    }
  }
}
