const webpack = require('webpack')
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const uglifyJsPlugin = webpack.optimize.UglifyJsPlugin

// sdk
const VERSION = require('./package.json').version
const IS_NPM = process.env.IS_NPM
const filePath = IS_NPM ? __dirname : path.join(__dirname, 'build', 'sdk')
const filename = IS_NPM ? 'sdk.js' : 'sdk-' + VERSION + '.js'

module.exports = {
  devtool: 'cheap-source-map',
  entry: [
    path.resolve(__dirname, 'src/main.js')
  ],
  output: {
    path: filePath,
    publicPath: '/',
    filename
  },
  module: {
    loaders: [
      { test: /\.js[x]?$/, include: path.resolve(__dirname, 'src'), exclude: /node_modules/, loader: 'babel-loader' }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new uglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new CopyWebpackPlugin([
      { from: './app/index.html', to: 'index.html' }
    ])
  ]
}
